# Copilot-Desktop

A desktop application for Copilot made with Electron.

> Copilot logo and name are trademarks of [Microsoft](https://copilot.com/).
> Icon extracted by [Wikipedia](https://en.m.wikipedia.org/wiki/File:Microsoft_365_Copilot_Icon.svg).

![Copilot](preview.png "Copilot")

## Install dependencies

This project uses [Electron](https://electronjs.org/) and [Electron Builder](https://www.electron.build/).

For install dependencies just make
- For install `Electron`
```
npm install electron -D
```
- For install `Electron-packager`
```
npm install electron-packager -D
```

## Test program
- Run this comand for test the application.
```
npm run test
```

## Build proyect
- Run this comand for build the application.
```
npm run build
```
