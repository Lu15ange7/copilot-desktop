// main.js

const { app, BrowserWindow } = require('electron');

function createWindow() {
  const win = new BrowserWindow({
    width: 430,
    height: 932,
    icon: 'icon.png', // Ruta al icono de la aplicación
    autoHideMenuBar: true, // Esta línea oculta la barra de menú
    webPreferences: {
      nodeIntegration: true, // Habilita el uso de Node.js en la ventana del navegador
    },
  });

  // Carga el archivo HTML de tu aplicación
  win.loadFile('index.html');

  // Abre las herramientas de desarrollo (opcional) (de preferencia desactivarlo)
//  win.webContents.openDevTools();
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
