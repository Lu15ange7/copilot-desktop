const fs = require("fs");
const path = require("path");

const dirsToRename = [
  { from: 'copilot-desktop-linux-x64', to: 'copilot-desktop-linux' },
  { from: 'copilot-desktop-linux-armv7l', to: 'copilot-desktop-linux' },
  { from: 'ccopilot-desktop-linux-arm64', to: 'copilot-desktop-linux' }
];

for (let dirObj of dirsToRename) {
  const oldPath = path.resolve(__dirname, 'release', dirObj.from);
  const newPath = path.resolve(__dirname, 'release', dirObj.to);

  if (fs.existsSync(oldPath)) {
    fs.renameSync(oldPath, newPath);
    console.log(`Renamed ${oldPath} => ${newPath}`);
  } else {
    console.warn(`Directory not found: ${oldPath}`);
  }
}